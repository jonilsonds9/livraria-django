from django.contrib import admin

from livraria.forms import CategoriaForm, EditoraForm, AutorForm
from livraria.models import Categoria, Editora, Autor


@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    form = CategoriaForm
    list_display = ('id', 'nome')
    list_display_links = ('id', 'nome')
    search_fields = ('nome',)
    list_per_page = 15
    ordering = ['id']


@admin.register(Editora)
class EditoraAdmin(admin.ModelAdmin):
    form = EditoraForm
    list_display = ('id', 'nome', 'telefone')
    list_display_links = ('id', 'nome')
    search_fields = ('nome',)
    list_per_page = 15
    ordering = ['id']


@admin.register(Autor)
class AutorAdmin(admin.ModelAdmin):
    form = AutorForm
    list_display = ('id', 'nome', 'email', 'telefone')
    list_display_links = ('id', 'nome', 'email')
    search_fields = ('nome', 'email',)
    list_per_page = 15
    ordering = ['id']
